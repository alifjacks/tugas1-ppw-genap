from django import forms

class Status_Form(forms.Form):
    '''
    error_messages = {
        'required' : 'Status can\'t be blank'

    }
    '''
    status_attrs = {
        'type': 'text',
        'rows': 2,
        'class': 'form-control status-box',
        'placeholder':'What\'s on your mind?'
    }

    new_status = forms.CharField(label='', required=True, max_length=280, widget=forms.Textarea(attrs=status_attrs))

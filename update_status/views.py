from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

def index(request) :
    response = {'content': "test"}
    status = Status.objects.all()
    response['status'] = status
    response['status_form'] = Status_Form
    response
    return render(request, 'status.html', response)


def status_post(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        status = request.POST['new_status']
        status = Status(new_status=status)
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

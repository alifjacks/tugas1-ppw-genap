from django.conf.urls import url
from .views import index, status_post

#url for app

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', status_post, name='add_status'),
]

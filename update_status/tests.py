from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import Status_Form

# Create your tests here.
class StatusPageUnitTest(TestCase):

    def test_status_page_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_status_page_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(new_status='THE SASH RINGING, THE TRASH THINGING, MASH FLINGING, THE FLASH SPRINGING, BRINGING THE CRASH THINGING')

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'new_status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['new_status'],
            ["This field is required."])

    def test_status_post_success_and_render_the_result(self):
        test = 'writing new status'
        response_post = Client().post('/status/add_status', {'new_status': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_status_post_error_and_render_the_result(self):
        test = 'writing new status'
        response_post = Client().post('/status/add_status', {'new_status': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


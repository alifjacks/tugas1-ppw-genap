from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from halaman_profile.views import profile_name
from update_status.models import Status
from add_friends.models import AddFriends
from .views import index
# Create your tests here.

class StatsPageUnitTest(TestCase):
	def test_stats_page_url_is_exist(self):
		response = Client().get('/app-stats/')
		self.assertEqual(response.status_code, 200)
	def test_stats_page_using_index_func(self):
		found = resolve('/app-stats/')
		self.assertEqual(found.func, index)
	def test_navbar_exists(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<nav', html_response)
	def test_already_have_post(self):
		msg = "hehehehe"
		newStatus = Status.objects.create(new_status=msg)
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn(msg, html_response)
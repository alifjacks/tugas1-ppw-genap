from django.shortcuts import render
from halaman_profile.views import profile_name
from update_status.models import Status
from add_friends.models import AddFriends


# Create your views here.
response = {}
def index(request):
    friend_count = AddFriends.objects.all().count()
    posts_count = Status.objects.all().count()

    response['nama'] = profile_name
    response['friend_count'] = friend_count
    response['posts_count'] = posts_count
    if(posts_count != 0):
        latest_status = Status.objects.latest('created_date')
        response['latest_status'] = latest_status
    return render(request, 'app_stats.html', response)
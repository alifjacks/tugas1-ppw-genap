from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from add_friends.views import index,add_friend,remove_friend
from add_friends.models import AddFriends
from add_friends.forms import AddFriendsForm

# Create your tests here.


class AddFriendsUnitTest(TestCase):

    def test_add_friends_is_exist(self):
        response = Client().get('/add-friends/')
        self.assertEqual(response.status_code, 200)
    
    def test_add_friends_index_func(self):
        found = resolve('/add-friends/')
        self.assertEqual(found.func, index)

    def test_model_can_added_friends(self):
        response = self.client.post('/add-friends/add_friend', data={'name': 'TEST', 'url' : 'http://berniaga.com'})
        counting_all_available_activity = AddFriends.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/add-friends/')
    
    def test_model_fail_added_friends(self):
        response = self.client.post('/add-friends/add_friend',  data={'name': '', 'url' : ''})
        counting_all_available_activity = AddFriends.objects.all().count()
        self.assertEqual(counting_all_available_activity, 0)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/add-friends/')
    
    def test_form_validation_for_blank_item(self):
        form = AddFriendsForm(data={'name': '','url': ''})
        self.assertFalse(form.is_valid())

    def test_remove_friend_from_list(self):
        response = self.client.post('/add-friends/add_friend', data={'name': 'TEST', 'url' : 'http://google.com'})
        id = AddFriends.objects.all().count()
        remove_friend(AddFriends,id)

        counting_all_available_activity = AddFriends.objects.all().count()
        self.assertEqual(counting_all_available_activity,0)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/add-friends/')

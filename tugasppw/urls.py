"""tugasppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import add_friends.urls as add_friends
import halaman_profile.urls as halaman_profile
import update_status.urls as update_status
import app_stats.urls as app_stats

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^add-friends/', include(add_friends,namespace='Friends Page')),
    url(r'^halaman_profile/', include(halaman_profile,namespace='Profile Page')),
    url(r'^', include(halaman_profile,namespace='index')),
    url(r'^status/', include(update_status, namespace='Status Page')),
    url(r'^app-stats/', include(app_stats, namespace='Stats Page')),
]
